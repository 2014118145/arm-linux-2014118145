﻿# 　韩山师范学院实验报告　 
### 姓名： 张伟滨　专业：软件工程
### 学号 : 2014118145　　　科目：嵌入式 Linux-ARM 应用开发　
###实验日期：2017.4.20
***
## 实验目的 ：
学会在C程序中分配内存

## 实验内容 ：
用malloc()函数、calloc()函数分配内存空间写入字符串，之后使用realloc()函数重新分配内存空间，最后释放动态分配的内存。程序在分配的内存空间内写入字符串，通过打印字符串到屏幕展示内存分配的结果。
### 实验源码
	#include <stdio.h>
	#include <stdlib.h>
	#include <string.h>
	int main() {
	    char *p_str1, *p_str2;  //定义两个char×指针
	
	    //使用malloc（）函数分配内存
	    p_str1 = (char*)malloc(32);
	    if (NULL == p_str1) {
	        printf("Alloc p_str1 memory ERROR!\n");
	        return -1;
	    }
	
	    //使用calloc()函数分配内存
	    p_str2 = (char*)calloc(32, sizeof(char));
	    if (NULL == p_str2){
	        printf("Alloc p_str2 memory ERROR!\n");
	        free(p_str1);
	        return -1;
	
	    }
	
	    strcpy(p_str1, "This is a simple sentence.");
	    strcpy(p_str2, p_str1);
	
	    //打印p_str1的结果
	    printf("p_str1 by malloc():\n");
	    printf("p_str1 address: 0x%.8x\n",p_str1);
	    printf("p_str1: %s(%d chars)\n", p_str1, strlen(p_str1));
	
	    //打印p_str2的结果
	    printf("p_str2 by calloc():\n");
	    printf("p_str2 address: 0x%.8x\n",p_str2);
	    printf("p_str2: %s(%d chars)\n",p_str2, strlen(p_str2));
	
	    //为p_str1重新分配内存（减小）
	    p_str1 = (char*)realloc(p_str1, 16);
	    if(NULL == p_str1){
	        printf("Realloc p_str1 memory ERROR!\n");
	        free(p_str2);
	        return -1;
	    }
	    p_str1[15] = '\0';
	
	    // 为p_str2重新分配内存（增大）
	    p_str2 = (char*)realloc(p_str2, 128);
	    if(NULL == p_str2){
	        printf("Realloc p_str2 memory ERROR!\n");
	        free(p_str1);
	        return -1;
	    }
	    strcat(p_str2, "The second sentence in extra memory after realloced!");
	
	    //打印p_str1的结果
	    printf("p_str1 after realloced\n");
	    printf("p_str1 address: 0x%.8x\n",p_str1);
	    printf("p_str1: %s(%d chars)\n",p_str1, strlen(p_str1));
	    printf("p_str1: %s(%d chars)\n",p_str1, strlen(p_str1));
	
	    //打印p_str2的结果
	    printf("p_str2 after realloced:\n");
	    printf("p_str2 address: 0x%.8x\n", p_str2);
	    printf("p_str2: %s(%d chars)\n", p_str2, strlen(p_str2));
	
	    //注意，最后要释放占用的内存
	    free(p_str1);
	    free(p_str2);
	
	    return 0;
	}


###实验结果
![结果][1]



[1]:https://bytebucket.org/Chen1st/arm-linux-2014118134/raw/220b3ed306bd55a7927e1147c7e6713577885249/%E5%AE%9E%E9%AA%8C%E4%BA%8C/%E8%BF%90%E8%A1%8C%E7%BB%93%E6%9E%9C.png