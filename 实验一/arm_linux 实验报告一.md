# 　韩山师范学院实验报告　 #
### 姓名： 张伟滨　　专业：软件工程
### 学号 : 2014118145  科目： 嵌入式 Linux-ARM 应用开发 实验日期：2107.03.12
***
## 实验目的 ：
1. 学会使用git和sourcetree 
2. 让代码进行更好的版本管理 

## 实验内容 ：
1. 用韩师邮箱注册bitbucket 账号并建立仓库
2. 建立team
3. Source本地克隆和云端

### 账号注册

![账号注册][1]


### bitbucket 云端仓库
![bitbucket 仓库][2]

### sourceTree 克隆仓库
![sourceTree 克隆仓库][3]


  [1]: https://coding.net/api/project/949466/files/1646984/imagePreview
  [2]: https://coding.net/api/project/949466/files/1647074/imagePreview
  [3]: https://coding.net/api/project/949466/files/1647077/imagePreview


####注： markdown  原链接 ：
#### https://coding.net/s/719ee87d-acf1-42bd-9cb9-fd54557bbeb0